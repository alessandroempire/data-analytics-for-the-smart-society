clear all; close all; clc;

%
% Written by: Alessandro La Corte
% ID: 100355025
%

% —------------------------------------------------------------------
% Image Classes
% —------------------------------------------------------------------

Num_img_classes = 4;
Classes = categorical({'cartman' 'cowboy-hat' 'galaxy' 'hibiscus'});


% —------------------------------------------------------------------
% TRAINING AND TEST SETS
% —------------------------------------------------------------------

TrSet_size = 61;
TestSet_size = 20;
Num_features_per_image = 1200;

% —------------------------------------------------------------------
% FEATURE EXTRACTION
% —------------------------------------------------------------------

load Xtrain;
load Xtest;

% —------------------------------------------------------------------
% CREATING A VISUAL VOCABULARY
% —------------------------------------------------------------------

Vocabulary_Size = 100;

%Loading the teachers vocabulary
load vocabulario_K100;


% Memory allocation for Histograms of visual words
Hist=zeros(Num_img_classes,TrSet_size,Vocabulary_Size);  


% —------------------------------------------------------------------
% Computing histograms
% —------------------------------------------------------------------

for i=1:Num_img_classes
    for j=1:TrSet_size
        
        % Visual words assignation
        image=(i-1)*TrSet_size+j;
        i1 = (image-1)*Num_features_per_image+1;
        i2 = i1 + Num_features_per_image -1;

        % Find to which clusters each features belongs to the visual words
        Cind_k = knnsearch(C, single(Xtrain(i1:i2,:)));
                
        % histogram computation
        Hist(i,j,:)=hist(Cind_k, 100);
        
        % Histogram normalization (sum=1)
        Hist(i,j,:)=Hist(i,j,:) / sum(Hist(i,j,:));        

    end
end


% —------------------------------------------------------------------
% TRAINING AN IMAGE CATEGORY CLASSIFIER
% —------------------------------------------------------------------

% Label Vector
Ytrain=[];
for i=1:Num_img_classes
    for j=1:TrSet_size
        Ytrain=[Ytrain; Classes(i)];
    end
end

% Reshaped Histogram Matrix
H_Xtrain=[];
for i=1:Num_img_classes
    for j=1:TrSet_size
        %Reshape H
        H_Xtrain=[H_Xtrain; reshape(Hist(i,j,:),1,Vocabulary_Size)];
    end
end

t = templateSVM('Standardize',true);
Classifier= fitcecoc(H_Xtrain,Ytrain,'Learners',t);

% —------------------------------------------------------------------
% Performance on the Training Set
% —------------------------------------------------------------------

% Predicted_Y_Train
Predicted_Y=predict(Classifier,H_Xtrain);

% confMatrix_Train
confMatrix = confusionmat(Ytrain,Predicted_Y)

% accuracy_Train
accuracy=sum(diag(confMatrix))/sum(confMatrix(:))






% --------------------------------------------------------------------
% REAL PERFORMANCE: PERFORMANCE ON THE TEST SET
% --------------------------------------------------------------------

% Memory allocation for Histograms of visual words
Hist_Test=zeros(Num_img_classes,TestSet_size,Vocabulary_Size);  

% --------------------------------------------------------------------
% Computing histograms
% --------------------------------------------------------------------

for i=1:Num_img_classes
    for j=1:TestSet_size
        
        % Visual words asignation
        image=(i-1)*TestSet_size+j;
        i1 = (image-1)*Num_features_per_image+1;
        i2 = i1 + Num_features_per_image -1;

        % Find to which clusters each features belongs to the visual words
        Cind_k=knnsearch(C,single(Xtest(i1:i2,:)));
       
        % histogram computation
        Hist_Test(i,j,:)=hist(Cind_k, 100);
        
        % Histogram normalization (sum=1)
        Hist_Test(i,j,:)=Hist_Test(i,j,:) / sum(Hist_Test(i,j,:)); 
    end
end



% --------------------------------------------------------------------
% Evaluation
% --------------------------------------------------------------------

% Label Vector: Y_Test
Ytest=[];
for i=1:Num_img_classes
    for j=1:TestSet_size
        Ytest=[Ytest; Classes(i)];
    end
end


% Reshaped Histogram Matrix: H_X_Test
H_X_Test=[];
for i=1:Num_img_classes
    for j=1:TestSet_size
        %Reshape
        H_X_Test=[H_X_Test; reshape(Hist_Test(i,j,:),1,Vocabulary_Size)];
    end
end


% Predicted_Y_Test
Predicted_Y=predict(Classifier,H_X_Test);

% confMatrix_Test
confMatrix = confusionmat(Ytest,Predicted_Y)

% accuracy_Test
accuracy=sum(diag(confMatrix))/sum(confMatrix(:))

