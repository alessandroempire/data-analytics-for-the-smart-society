
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% Alessandro La Corte 100355025 %%%%%%%%%%
%%%%%%%%%% Speech recognition Lab-2      %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% TASK 1 
% This task consists of the implementation in Matlab of the speaker identification system
% described in Section 2. The following steps have to be done:

for i=1:16
    
    % Let's load the files for each speaker
    train = load_train_data('list_train.txt', i);
      
    % Let's begin with feature extraction
    train_features = melfcc(train,16000, 'wintime', 0.02, 'hoptime', 0.01, 'numcep', 20);
      
    % Speaker GMM models building for each speaker
    % GM distributions are stored into the matrix 'models'
    models{i} = fitgmdist(train_features',8,'CovType','diagonal', 'Replicates', 10);     
end 

% Now, lets test the model. 
% First, let's start with the clean records
id             = fopen('list_test1.txt');  
speech_info    = textscan(id, '%s%f');
speech_length  = length(speech_info{1});   
clean_speakers = int16(speech_info{2});    
fclose(id);

%Let's create an array to store the predicted speakers ids
predicted_clean_speakers = zeros(speech_length,1);
for i=1:speech_length,

    % 1. Read the speeches
    clean_speeches = audioread(speech_info{1}{i});

    % 2. Feature extraction
    clean_features = melfcc(clean_speeches, 16000, 'wintime', 0.02, 'hoptime', 0.01, 'numcep', 20);
    
    % 3. Log-likelihood computation
    for j=1:16,
        LL(j) = sum(log(pdf(models{j},clean_features')));
    end
    
    % 4. Define the speaker by MAX log-likelihood
    predicted_clean_speakers(i) = find(LL==max(LL));
end

% Now, let's do the same but with noisy records. 
id             = fopen('list_test2.txt'); 
speech_info    = textscan(id, '%s%f');
speech_length  = length(speech_info{1}); 
noisy_speakers = int16(speech_info{2});   
fclose(id);

%Let's create an array to store the predicted speakers ids
predicted_noisy_speakers = zeros(speech_length,1); 
for i=1:speech_length,
    
    % 1. Read the speeches
    noisy_speeches = audioread(speech_info{1}{i});
    
    % 2. Feature extraction
    noisy_features = melfcc(noisy_speeches, 16000, 'wintime', 0.02, 'hoptime', 0.01, 'numcep', 20);
    
    % 3. Log-likelihood computation
    for j=1:16,
        LL(j) = sum(log(pdf(models{j},noisy_features'))); %remember that feat_i' needs to be inverted with '
    end
    
    % 4. Define the speaker by MAX log-likelihood
    predicted_noisy_speakers(i) = find(LL==max(LL));
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TASK 2 
% This task consists of the evaluation of the baseline system implemented in Task 1, in
% both, clean and noisy conditions. In all cases, the performance of the speaker
% identification system is measured in terms of the identification accuracy (percentage of
% correctly identified test files with respect to the total number of test files)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The clean records
correct = 0;
for i=1:speech_length,
   if clean_speakers(i) == predicted_clean_speakers(i)
       correct = correct + 1;
   end
end
cleanrecords_accuracy = (correct/speech_length)*100

% The noisy records
correct2 = 0;
for i=1:speech_length,
   if noisy_speakers(i) == predicted_noisy_speakers(i)
       correct2 = correct2 + 1;
   end
end
noisyrecords_accuracy = (correct2/speech_length)*100
      

